package com.epam;

import com.epam.minesweeper.Minesweeper;

public class App {

    public static void main(String[] args) {
        Minesweeper minesweeper = new Minesweeper(10, 10, 10);
        minesweeper.printField();
        System.out.println();
        minesweeper.printSolution();
    }

}
