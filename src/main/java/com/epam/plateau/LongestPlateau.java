package com.epam.plateau;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

// TODO: Add tests
public class LongestPlateau {

    private List<Integer> list;
    private List<Plateau> plateaus;

    public LongestPlateau() {
        plateaus = new ArrayList<>();
    }

    public LongestPlateau(List<Integer> list) {
        this();
        this.list = list;
    }

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    public void findSequence() {
        int start = 0;
        int end = 0;
        for (int i = 0; i < list.size(); i++) {
            start = i;
            end = start;
            if ((i + 1 < list.size()) && list.get(i).equals(list.get(i + 1))) {
                for (int j = i + 1; j < list.size(); j++) {
                    if ( ! list.get(j).equals(list.get(start))) {
                        break;
                    }
                    end++;
                }
                i = end;
                if ((start != 0 || end != list.size()) && checkEdgeElements(list, start, end)) {
                    plateaus.add(new Plateau(start, end));
                }
            }
        }
    }

    public boolean checkEdgeElements(List<Integer> list, int start, int end) {
        return list.get(start - 1) < list.get(start) && list.get(end + 1) < list.get(start);
    }

    public Plateau getPlateau() {
        return plateaus.stream().max(Comparator.comparingInt(Plateau::getLength)).orElse(null);
    }

}
