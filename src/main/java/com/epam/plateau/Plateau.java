package com.epam.plateau;

public class Plateau {

    private int start;
    private int end;
    private int length;

    public Plateau() {
    }

    public Plateau(int start, int end) {
        this.start = start;
        this.end = end;
        this.length = end - start + 1;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return "Plateau{" +
                "start=" + start +
                ", end=" + end +
                ", length=" + length +
                '}';
    }

}
