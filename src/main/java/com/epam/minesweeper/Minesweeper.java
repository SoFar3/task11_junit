package com.epam.minesweeper;

import java.util.Random;

public class Minesweeper {

    private int mineProbability;
    private boolean[][] field;

    private Random random;

    public Minesweeper(boolean[][] field) {
        this.random = new Random();
        this.field = field;
    }

    public Minesweeper(int M, int N) {
        this(M, N, 10);
    }

    public Minesweeper(int M, int N, int mineProbability) {
        this.random = new Random();
        this.mineProbability = mineProbability;
        this.field = new boolean[M][N];

        generateMine();
    }

    public void printSolution() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                int i1 = countMines(i, j);
                System.out.print(String.format("%-3s", i1 == 0 ?  ".": i1 == -1 ? "*" : i1 + ""));
            }
            System.out.println();
        }
    }

    public void printField() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[0].length; j++) {
                System.out.print(String.format("%-3s", field[i][j] ? "*" : "."));
            }
            System.out.println();
        }
    }

    public int countMines(int row, int col) {
        int count = 0;
        if (! field[row][col]) {
            for (int i = row - 1; i <= row + 1; i++) {
                for (int j = col - 1; j <= col + 1; j++) {
                    if ((i >= 0 && i < field.length) && (j >= 0 && j < field[0].length)) {
                        if (field[i][j]) {
                            count++;
                        }
                    }
                }
            }
        } else {
            return -1;
        }
        return count;
    }

    private void generateMine() {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                field[i][j] = random.nextInt(100) <= mineProbability;
            }
        }
    }

    public boolean[][] getField() {
        return field;
    }

}
