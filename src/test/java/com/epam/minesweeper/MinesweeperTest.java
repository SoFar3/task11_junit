package com.epam.minesweeper;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

class MinesweeperTest {

    private static boolean[][] field;
    private static Minesweeper minesweeper;

    @BeforeAll
    static void init() {
        field = new boolean[][] {
                {true, false, false, false, false, false, true},
                {false, false, false, false, false, true, false},
                {false, false, false, false, false, false, false},
                {false, true, true, true, true, false, false},
                {true, false, false, false, false, false, true},
                {false, false, false, false, true, false, false},
                {true, false, false, false, false, false, false}
        };

       minesweeper = new Minesweeper(field);
    }

    @ParameterizedTest
    @CsvSource({"5, 5", "4, 4", "7, 8"})
    void fieldSizeTest(int M, int N) {
        Minesweeper minesweeper = new Minesweeper(M, N);

        assertEquals(minesweeper.getField().length, M);
        assertEquals(minesweeper.getField()[0].length, N);
    }

    @Test
    void printFieldTest() {
        minesweeper.printField();
    }

    @Test
    void printSolutionTest() {
        minesweeper.printSolution();
    }

    @ParameterizedTest
    @CsvSource({"0, 0, -1", "0, 1, 1", "2, 3, 3"})
    void mineCountingTest(int i, int j, int amount) {
        int i1 = minesweeper.countMines(i, j);
        assertTrue(i1 == amount);
    }

}
